extern crate cc;

use std::env;
use std::path::Path;

const INCLUDE_PATH: &str = "include";
const MEMCPY_PATH: &str = "src/memcpy_s.c";
const MEMSET_PATH: &str = "src/memset_s.c";
const LZ4_CPATH: &str = "lz4.c";

const BCF_PATH: &str = "third_party/bounds_checking_function";
const LZ4_PATH: &str = "third_party/lz4/lib/";

fn main() {
    let oh_path = env::var_os("OH_DIR");

    let bcf_path = match env::var_os("BCF_DIR") {
        Some(path) => path,
        None => {
            if let Some(path) = oh_path.clone() {
                Path::new(&path).join(BCF_PATH).as_os_str().to_owned()
            } else {
                return;
                //panic!("neither [BCF_DIR=/path/to/bounds_checking_function] nor [OH_DIR] is not found in env");
            }
        }
    };

    let lz4_src = if let Some(path) = oh_path.clone() {
        Path::new(&path).join(LZ4_PATH).as_os_str().to_owned()
    } else {
        panic!("[OH_DIR] is not found in env");
    };

    let bcf_header = Path::new(&bcf_path).join(INCLUDE_PATH);
    let memcpy_src = Path::new(&bcf_path).join(MEMCPY_PATH);
    let memset_src = Path::new(&bcf_path).join(MEMSET_PATH);
    let lz4_src_cpp = Path::new(&lz4_src).join(LZ4_CPATH).clone();

    let cpp_files = vec![
        memcpy_src.to_str().unwrap(),
        memset_src.to_str().unwrap(),
        lz4_src_cpp.to_str().unwrap(),
        "src/cffi/serial_struct.cpp",
        "src/cffi/uart.cpp",
        "src/cffi/uart_wrapper.cpp",
        "src/cffi/usb_util.cpp",
        // 用了linux的头文件，需要windows适配
        //"src/cffi/oh_usb.cpp",
        //"src/cffi/usb_wrapper.cpp",
        //"src/cffi/mount.cpp",
        //"src/cffi/mount_wrapper.cpp",
        "src/cffi/transfer.cpp",
    ];

    let mut build = cc::Build::new();
    build
        .include(bcf_header)
        .include("/usr/include/")
        .include("/usr/lib/gcc/x86_64-linux-gnu/9/include/")
        .include(lz4_src)
        .files(cpp_files)
        .define("HOST_MINGW","")
        .cpp(true);

    if cfg!(target_os = "macos") {
        build.flag("-D HOST_MAC");
    } else if cfg!(target_os = "linux") {
        build.flag("-D HOST_LINUX");
    }

    build.compile("serial_struct");

    if cfg!(target_os = "windows") {
        const MINGW_PATH: &str =
            "prebuilts/mingw-w64/ohos/linux-x86_64/clang-mingw/x86_64-w64-mingw32";
        const WINDEP_SRC: &str = "src/cffi/win_dep.cpp";
        let mingw_path = match env::var_os("MINGW_DIR") {
            Some(path) => path,
            None => {
                if let Some(path) = oh_path {
                    Path::new(&path).join(MINGW_PATH).as_os_str().to_owned()
                } else {
                    panic!("neither [MINGW_DIR=/path/to/x86_64-w64-mingw32] nor [OH_DIR] is not found in env");
                }
            }
        };

        let include_dep = Path::new(&mingw_path).join(INCLUDE_PATH);
        cc::Build::new()
            .include(include_dep)
            .file(WINDEP_SRC)
            .cpp(true)
            .flag("--std=c++17")
            .compile("win_dep");
    }
}
