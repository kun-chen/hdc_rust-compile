# hdc_rust cargo编译——**winsdows**
## 1、安装MSYS2及rust
1）参考链接依次执行**步骤1**至**步骤5**安装MSYS2
https://zhuanlan.zhihu.com/p/237932497

2）如已经安装过rust，需要**卸载rust后按照步骤5重新安装**，卸载命令如下
```
rustup self uninstall
```
3）在carog.exe的**上一级目录**下创建cargo的config文件，文件内容如下
```
[target.x86_64-pc-windows-gnu]
linker = "C:\\msys64\\mingw64\\bin\\gcc.exe"
ar = "C:\\msys64\\mingw64\\bin\\ar.exe"
```

4）安装完成后打开**MSYS2 MINGW64**终端，保证能执行cargo命令即可

![alt text](images/image-1.png)

备注：安装MSYS2的原因，windows平台mingw下使用openssl建议安装MSYS2环境
https://github.com/sfackler/rust-openssl/tree/5948898e54882c0bedd12d87569eb4dbee5bbca7#windows-msvc

![rust-openssl README](images/image-2.png)

## 2、进入MSYS MINGW64的终端，执行下述命令安装openssl
```pacman -S mingw-w64-x86_64-openssl```

![alt text](images/image-3.png)

## 3、修改代码
#### 3.1 从本仓拷贝cargo工程清单Cargo.toml和构建脚本build.rs至hdc_rust目录下
![cargo清单和构建脚本](images/image-4.png)

1）build.rs临时规避如下，后续完善代码后可以放开编译

cffi中usb和mount cpp文件未适配windows，需要修改build.rs临时屏蔽和增加编译宏HOST_MINGW
```
         "src/cffi/uart.cpp",
         "src/cffi/uart_wrapper.cpp",
         "src/cffi/usb_util.cpp",
-        "src/cffi/oh_usb.cpp",
-        "src/cffi/usb_wrapper.cpp",
-        "src/cffi/mount.cpp",
-        "src/cffi/mount_wrapper.cpp",
+        // 用了linux的头文件，需要windows适配，临时屏蔽
+        //"src/cffi/oh_usb.cpp",
+        //"src/cffi/usb_wrapper.cpp",
+        //"src/cffi/mount.cpp",
+        //"src/cffi/mount_wrapper.cpp",
         "src/cffi/transfer.cpp",
     ];

@@ -58,6 +59,7 @@ fn main() {
         .include("/usr/lib/gcc/x86_64-linux-gnu/9/include/")
         .include(lz4_src)
         .files(cpp_files)
         // 增加HOST_MINGW，用于cffi中的cpp代码编译宏隔离
+        .define("HOST_MINGW","")
         .cpp(true);

     if cfg!(target_os = "macos") {

```
2）Cargo.toml文件中hilog_rust的依赖，**path路径需要根据实际目录适配修改**
```
[dependencies]
ylong_runtime = { git = "https://gitee.com/openharmony/commonlibrary_rust_ylong_runtime.git", features = ["full"] }
nix = { version = "0.26.2", optional = true }
lazy_static = { version = "1.4.0", optional = false }
libc = "0.2.139"
log = "0.4.17"
env_logger = { version = "0.9.3", optional = true }
humantime = "2.1.0"
openssl = { version = "0.10.47", features = ["vendored"] }
hilog_rust = { path = "D:/code/ohos/base/hiviewdfx/hilog/interfaces/rust" }
```
#### 3.2 从本仓获取hdc_rust编译依赖的oh代码ohos.zip，解压放到hdc_rust同级目录下
![alt text](images/image-7.png)
![alt text](images/image-6.png)
![alt text](images/image-5.png)

#### 3.3 hdc_rust代码拉取最新下述PR中的代码
https://gitee.com/openharmony/developtools_hdc/pulls/954
```
feat:solve problems of compile hdc_rust for windows and linux
1)usb & uart interface blocking temporarily, need to fix later(others)
2)change windows hdc server listen ip to 127.0.0.1 for the connect problem(parser.rs)
3)solve the windows hilog panic problem(logger.rs)
4)solve linux client connect server problems(server.rs)
```

## 4、执行下述命令开始编译
debug版本：
```
OH_DIR=/d/code/ohos cargo build --bin hdc_rust --features=host --target x86_64-pc-windows-gnu
```
release版本：
```
OH_DIR=/d/code/ohos cargo build --bin hdc_rust --features=host --target x86_64-pc-windows-gnu **--release**
```
注：
1)OH_DIR路径需要根据实际代码路径修改配置
2)首次编译openssl_sys时耗时约20min，需要耐心等待，第二次修改代码后不执行cargo clean或者不删除target则无需重复编译openssl


# hdc_rust cargo编译——**linux**


## 1、拷贝Cargo.toml和build.rs至hdc_rust/目录下
Cargo.toml和build.rs 取自 https://gitee.com/leiguangyu/hdc_rust/tree/master 仓

## 2、Cargo.toml中[dependencies]增加关于hilog_rust的路径配置，根目录根据蓝区环境修改
hilog_rust = { path = "/home/kun/ohos_blue/base/hiviewdfx/hilog/interfaces/rust" }

## 3、执行下述命令编译
```
OH_DIR=/home/kun/ohos_blue cargo build --bin hdc_rust --features=host
```

# 其他参考
https://github.com/wez/wezterm/issues/731
https://stackoverflow.com/questions/55912871/how-to-work-with-openssl-for-rust-within-a-windows-development-environment
https://github.com/sfackler/rust-openssl/tree/5948898e54882c0bedd12d87569eb4dbee5bbca7#windows-msvc
