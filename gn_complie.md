# 使用gn编译rust语言的hdc
## 1、首次编译，全量更新代码（重点是build仓/ylong_runtime仓及hdc仓），执行./build/prebuilts_download.sh更新工具链
```
repo sync -c
./build/prebuilts_download.sh

```
## 2、修改build仓中ohos_sdk_description_std.json的配置指定编译hdc_rust
首次编译sdk时因为配置组件多耗时较长，需要快速验证编译可以删除ohos_sdk_description_std.json中的其他配置，仅保留如下配置
**//build/ohos/sdk/ohos_sdk_description_std.json：**
```
     {
         "install_dir": "toolchains",
         "module_label": "//developtools/hdc/hdc_rust:hdc_rust",
         "target_os": [
             "linux",
             "windows",
         ]
     }
```


## 3、蓝区执行sdk编译命令编译
执行下述命令编译sdk

time ./build.sh --product-name ohos-sdk --ccache

## 4、编译的hdc_rust在如下路径，同时需要在out目录下取hdc_rust依赖的三个动态库libcrypto_openssl.dll、libssl_openssl.dll、libusb_shared.dll
![输入图片说明](images/image001.png)
![输入图片说明](images/image.png)

